package sscsweb1617.trackitup;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import sscsweb1617.trackitup.model.Delivery;

class DeliveryAdapter extends RecyclerView.Adapter<DeliveryAdapter.DeliveryViewHolder> {
    private List<Delivery> list;
    private String cookie;

    DeliveryAdapter(String cookie) {
        list = new ArrayList<>();
        this.cookie = cookie;
    }

    @Override
    public DeliveryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.delivery_item, parent, false);

        return new DeliveryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DeliveryViewHolder holder, int position) {
        LatLng dest = list.get(position).getDestination();
        String destText = String.format(Locale.getDefault(), "%.4f lat\n%.4f lng", dest.latitude, dest.longitude);

        holder.textParcel.setText(list.get(position).getParcel());
        holder.textDrone.setText(list.get(position).getDrone());
        holder.textDestination.setText(destText);
        holder.itemView.setOnClickListener(
                new OnDeliveryItemClickListener(list.get(position).getId()));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    void setItems(List<Delivery> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    class DeliveryViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        TextView textParcel;
        TextView textDrone;
        TextView textDestination;

        DeliveryViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;
            this.textParcel = itemView.findViewById(R.id.textParcel);
            this.textDrone = itemView.findViewById(R.id.textDrone);
            this.textDestination = itemView.findViewById(R.id.textDestination);
        }
    }

    private class OnDeliveryItemClickListener implements View.OnClickListener {
        private long deliveryId;

        OnDeliveryItemClickListener(long deliveryId) {
            this.deliveryId = deliveryId;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), DeliveryMapActivity.class);
            intent.putExtra("delivery", deliveryId);
            intent.putExtra("JSESSIONID", cookie);
            view.getContext().startActivity(intent);
        }
    }
}
