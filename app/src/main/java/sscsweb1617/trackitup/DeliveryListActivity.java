package sscsweb1617.trackitup;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import sscsweb1617.trackitup.model.Delivery;

public class DeliveryListActivity extends AppCompatActivity {
    private String cookie;
    private DeliveryAdapter adapter;

    private RecyclerView recyclerView;
    private TextView textEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_list);

        Intent i = getIntent();
        cookie = i.getStringExtra("JSESSIONID");
        adapter = new DeliveryAdapter(cookie);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(
                new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        textEmpty = (TextView) findViewById(R.id.textEmpty);

        TextView textName = (TextView) findViewById(R.id.textName);
        textName.setText(i.getStringExtra("name"));

        TextView textEmail = (TextView) findViewById(R.id.textEmail);
        textEmail.setText(i.getStringExtra("email"));
    }

    @Override
    protected void onStart() {
        super.onStart();
        new DeliveryListTask(recyclerView, textEmpty).execute(cookie);
    }

    private class DeliveryListTask extends AsyncTask<String, Void, JSONArray> {
        private final String REST_URL = "http://10.0.2.2:8080/pickitup-war/webresources/deliveries/";

        private ProgressBar progressBar;
        private TextView textEmpty;
        private RecyclerView recyclerView;

        DeliveryListTask(RecyclerView recyclerView, TextView textEmpty) {
            progressBar = (ProgressBar) findViewById(R.id.listProgressBar);
            this.recyclerView = recyclerView;
            this.textEmpty = textEmpty;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONArray doInBackground(String... cookie) {
            try {
                // Creazione connessione
                URL url = new URL(REST_URL);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Cookie", "JSESSIONID=" + cookie[0]);

                // Ottenimento risultato
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    throw new IOException(connection.getResponseMessage());
                }
                InputStream in = new BufferedInputStream(
                        connection.getInputStream());
                String response = new Scanner(in).useDelimiter("\\A").next();
                Log.d("REST", response);

                return new JSONArray(response);
            } catch (IOException | JSONException e) {
                JSONArray errorArray = new JSONArray();
                try {
                    JSONObject error = new JSONObject();
                    error.put("error", e.getMessage());
                    errorArray.put(error);
                } catch (JSONException ee) {/* No problem*/}
                return errorArray;
            }
        }

        @Override
        protected void onPostExecute(JSONArray response) {
            super.onPostExecute(response);

            try {
                if (response.length() == 1 && response.getJSONObject(0).has("error")) {
                    new AlertDialog.Builder(DeliveryListActivity.this)
                            .setTitle(R.string.connection_failed)
                            .setMessage(response.getJSONObject(0).getString("error"))
                            .show();
                } else {
                    List<Delivery> deliveries = jsonToList(response);
                    if(deliveries.isEmpty()) {
                        recyclerView.setVisibility(View.GONE);
                        textEmpty.setVisibility(View.VISIBLE);
                    }
                    else {
                        adapter.setItems(deliveries);
                    }
                }
            } catch (JSONException e) {/* Pas grave */}

            progressBar.setVisibility(View.INVISIBLE);
        }

        private List<Delivery> jsonToList(JSONArray json) {
            List<Delivery> deliveries = new ArrayList<>();
            try {
                for (int i = 0; i < json.length(); i++) {
                    JSONObject jsonDelivery = json.getJSONObject(i);

                    Delivery d = new Delivery();
                    d.setParcel(jsonDelivery.getString("name"));
                    d.setDrone(jsonDelivery.getString("drone"));
                    d.setId(jsonDelivery.getLong("id"));

                    JSONObject latLng = jsonDelivery.getJSONObject("destination");
                    LatLng pos = new LatLng(latLng.getDouble("lat"), latLng.getDouble("lng"));
                    d.setDestination(pos);

                    deliveries.add(d);
                }
            } catch (JSONException e) {
                // Pazienza...
            }
            return deliveries;
        }
    }
}
