package sscsweb1617.trackitup.model;

import com.google.android.gms.maps.model.LatLng;

public class Delivery {
    private LatLng clientPosition;
    private LatLng dronePosition;
    private LatLng destination;
    private String parcel;
    private String drone;
    private long id;

    public Delivery() {
        id = -1;
    }

    @Override
    public String toString() {
        return getTitle();
    }

    public String getParcel() {
        return parcel;
    }

    public String getDrone() {
        return drone;
    }

    public LatLng getDestination() {
        return destination;
    }

    public long getId() {
        return id;
    }

    public LatLng getClientPosition() {
        return clientPosition;
    }

    public LatLng getDronePosition() {
        return dronePosition;
    }

    private LatLng randomPosition() {
        double lat = 45.0092 + (45.117332 - 45.0092) * Math.random();
        double lng = 7.698594 + (7.591389 - 7.698594) * Math.random();

        return new LatLng(lat, lng);
    }

    public String getTitle() {
        return "Delivery no. " + (int) (200 * Math.random());
    }

    public String getDescription() {
        return "blablabla\nbshdsuf\nspeedblabla";
    }

    public void setParcel(String parcel) {
        this.parcel = parcel;
    }

    public void setDrone(String drone) {
        this.drone = drone;
    }

    public void setDestination(LatLng destination) {
        this.destination = destination;
    }

    public void setId(long id) {
        this.id = id;
    }
}