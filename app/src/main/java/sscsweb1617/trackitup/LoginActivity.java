package sscsweb1617.trackitup;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class LoginActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private static final int GOOGLE_SIGN_IN = 0;

    private GoogleApiClient mGoogleApiClient;
    private CallbackManager facebookCallbackManager;
    private ProfileTracker facebookProfileTracker;
    private String facebookAccessToken;

    @Override
    protected void onResume() {
        super.onResume();
        // Logs out from Facebook / Google
        LoginManager.getInstance().logOut();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        facebookProfileTracker.stopTracking();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setupGoogleLogin();
        setupFacebookLogin();
    }

    private void setupFacebookLogin() {
        // Callback per facebook
        facebookCallbackManager = CallbackManager.Factory.create();
        facebookProfileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile old,
                    Profile profile) {
                if (profile != null) {
                    new LoginTask(LoginType.FACEBOOK).execute(facebookAccessToken);
                }
            }
        };
        LoginManager.getInstance().registerCallback(facebookCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        facebookAccessToken = loginResult.getAccessToken().getToken();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginActivity.this, R.string.facebook_login_canceled,
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(LoginActivity.this, exception.getLocalizedMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void setupGoogleLogin() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.google_server_client_id))
                .requestEmail()
                .build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        SignInButton googleSignInButton = (SignInButton) findViewById(R.id.google_login);
        googleSignInButton.setOnClickListener(this);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, R.string.connection_failed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.google_login:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, GOOGLE_SIGN_IN);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSignInResult(result);
        } else {
            facebookCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleGoogleSignInResult(GoogleSignInResult result) {
        Log.d(getLocalClassName(), "googleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            new LoginTask(LoginType.GOOGLE).execute(acct.getIdToken());
        } else {
            // Signed out, show unauthenticated UI.
            Toast.makeText(this, R.string.login_failed, Toast.LENGTH_SHORT).show();
        }
    }

    private class LoginTask extends AsyncTask<String, Void, JSONObject> {
        private final String REST_URL = "http://10.0.2.2:8080/pickitup-war/webresources/login/";
        private final String FACEBOOK_LOGIN = "facebook/";
        private final String GOOGLE_LOGIN = "google/";

        private String loginTypeStr;
        private ProgressBar progressBar;

        LoginTask(LoginType loginType) {
            switch (loginType) {
                case FACEBOOK:
                    loginTypeStr = FACEBOOK_LOGIN;
                    break;
                case GOOGLE:
                    loginTypeStr = GOOGLE_LOGIN;
                    break;
                default:
                    throw new IllegalArgumentException();
            }
            progressBar = (ProgressBar) findViewById(R.id.loginProgressBar);
        }

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONObject doInBackground(String... token) {
            try {
                // Creazione connessione
                URL url = new URL(REST_URL + loginTypeStr + token[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestMethod("GET");

                // Ottenimento risultato
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    throw new IOException(connection.getResponseMessage());
                }
                InputStream in = new BufferedInputStream(
                        connection.getInputStream());
                String response = new Scanner(in).useDelimiter("\\A").next();
                Log.d("REST", response);

                return new JSONObject(response);
            } catch (IOException | JSONException e) {
                JSONObject error = new JSONObject();
                try {
                    error.put("error", e.getMessage());
                } catch (JSONException e1) {
                    // No problem
                }
                return error;
            }
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            try {
                if (response.has("error")) {
                    new AlertDialog.Builder(LoginActivity.this)
                            .setTitle(R.string.connection_failed)
                            .setMessage(response.getString("error"))
                            .show();
                } else {
                    Intent i = new Intent(LoginActivity.this, DeliveryListActivity.class);
                    i.putExtra("JSESSIONID", response.getString("JSESSIONID"));
                    i.putExtra("email", response.getString("email"));
                    i.putExtra("name", response.getString("name"));

                    startActivity(i);
                }
            } catch (JSONException e) {
                Log.e("webservice", e.getMessage());
                e.printStackTrace();
            } finally {
                progressBar.setVisibility(View.INVISIBLE);
            }
        }
    }
}
