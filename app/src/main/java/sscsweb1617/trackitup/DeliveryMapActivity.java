package sscsweb1617.trackitup;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class DeliveryMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_map);

        // Per qualche motivo non la caricava sull'XML
        ImageView img = findViewById(R.id.imageDelivery);
        img.setImageResource(R.drawable.dest_marker);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        long deliveryId = getIntent().getLongExtra("delivery", -1);
        if (deliveryId == -1) {
            throw new IllegalStateException("No delivery selected!");
        }
        String cookie = getIntent().getStringExtra("JSESSIONID");

        ProgressBar progressBar = findViewById(R.id.deliveryProgress);
        TextView textDeliveryTitle = findViewById(R.id.textDeliveryTitle);
        TextView textDeliveryDescription = findViewById(R.id.textDeliveryDescription);
        new DeliveryTask(progressBar, textDeliveryTitle, textDeliveryDescription)
                .execute("" + deliveryId, cookie);
    }

    private class DeliveryTask extends AsyncTask<String, Void, JSONObject> {
        private final String REST_URL = "http://10.0.2.2:8080/pickitup-war/webresources/delivery/";

        private ProgressBar progressBar;
        private TextView textDeliveryTitle;
        private TextView textDeliveryDescription;

        DeliveryTask(ProgressBar progressBar, TextView textDeliveryTitle, TextView textDeliveryDescription) {
            this.progressBar = progressBar;
            this.textDeliveryTitle = textDeliveryTitle;
            this.textDeliveryDescription = textDeliveryDescription;
        }

        @Override
        protected JSONObject doInBackground(String... params) {

            try {
                // Creazione connessione
                URL url = new URL(REST_URL + params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                connection.setRequestProperty("Accept", "application/json");
                connection.setRequestProperty("Cookie", "JSESSIONID=" + params[1]);

                // Ottenimento risultato
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    throw new IOException(connection.getResponseMessage());
                }
                InputStream in = new BufferedInputStream(
                        connection.getInputStream());
                String response = new Scanner(in).useDelimiter("\\A").next();
                Log.d("REST", response);

                return new JSONObject(response);
            } catch (IOException | JSONException e) {
                JSONObject error = new JSONObject();
                try {
                    error.put("error", e.getMessage());
                } catch (JSONException ee) {/* No problem*/}
                return error;
            }
        }

        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);
            try {
                if (response.has("error")) {
                    new AlertDialog.Builder(DeliveryMapActivity.this)
                            .setTitle(R.string.connection_failed)
                            .setMessage(response.getString("error"))
                            .show();
                } else {
                    updateDeliveryDescription(response);
                    updateDeliveryMap(response);
                }
            } catch (JSONException e) {/* Pas grave */}

            progressBar.setVisibility(View.GONE);
        }

        private void updateDeliveryMap(JSONObject response) throws JSONException {
            JSONObject initialPosition = response.getJSONObject("initial_position");
            double initialLat = initialPosition.getDouble("lat");
            double initialLng = initialPosition.getDouble("lng");
            LatLng initialLatLng = new LatLng(initialLat, initialLng);

            JSONObject acceptancePosition = response.getJSONObject("acceptance_position");
            double acceptanceLat = acceptancePosition.getDouble("lat");
            double acceptanceLng = acceptancePosition.getDouble("lng");
            LatLng acceptanceLatLng = new LatLng(acceptanceLat, acceptanceLng);

            JSONObject destination = response.getJSONObject("destination");
            double destinationLat = destination.getDouble("lat");
            double destinationLng = destination.getDouble("lng");
            LatLng destinationLatLng = new LatLng(destinationLat, destinationLng);

            populateMap(initialLatLng, acceptanceLatLng, destinationLatLng);
        }

        private void populateMap(LatLng initialLatLng, LatLng acceptanceLatLng, LatLng destinationLatLng) {
            MarkerOptions clientMarker = new MarkerOptions()
                    .position(initialLatLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.client_marker));
            MarkerOptions droneMarker = new MarkerOptions()
                    .position(acceptanceLatLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.drone_marker));
            MarkerOptions destinationMarker = new MarkerOptions()
                    .position(destinationLatLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.dest_marker));
            PolylineOptions pathLine = new PolylineOptions()
                    .add(initialLatLng)
                    .add(acceptanceLatLng)
                    .add(destinationLatLng);

            mMap.addMarker(clientMarker);
            mMap.addMarker(droneMarker);
            mMap.addMarker(destinationMarker);
            mMap.addPolyline(pathLine);

            LatLngBounds bounds = LatLngBounds.builder()
                    .include(initialLatLng)
                    .include(acceptanceLatLng)
                    .include(destinationLatLng)
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 150));
        }

        private void updateDeliveryDescription(JSONObject response) throws JSONException {
            String name = response.getString("name");

            String client = response.getString("client");
            String drone = response.getString("drone");
            String statusDesc = response.getString("status_desc");
            double cost = response.getDouble("cost");
            int estimatedTime = response.getInt("estimated_time");
            String description = getString(R.string.delivery_description,
                    client, drone, statusDesc, cost, estimatedTime);

            textDeliveryTitle.setText(name);
            if(Build.VERSION.SDK_INT < 24) {
                textDeliveryDescription.setText(Html.fromHtml(description));
            }
            else {
                textDeliveryDescription.setText(Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT));
            }
        }
    }
}
